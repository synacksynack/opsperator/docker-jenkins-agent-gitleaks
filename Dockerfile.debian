FROM docker.io/debian:buster-slim

ENV GITLEAKS_VERSION=8.8.4 \
    GITLEAKS_REPO=https://github.com/zricethezav/gitleaks/releases/download

LABEL com.redhat.component="tekton-agent-gitleaks-buster-container" \
      io.k8s.description="The tekton agent gitleaks image has the tools scanning Git repositories for secrets." \
      io.k8s.display-name="Tekton Agent - GitLeaks" \
      io.openshift.tags="openshift,tekton,agent,gitleaks" \
      name="ci/tekton-agent-gitleaks" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-gitleaks" \
      version="1.0.4"

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	apt-get -y upgrade; \
    fi \
    && apt-get -y install curl ca-certificates jq \
    && if uname -m | grep aarch64 >/dev/null; then \
	curl -fsL -o /tmp/gitleaks.tar.gz \
	    $GITLEAKS_REPO/v$GITLEAKS_VERSION/gitleaks_${GITLEAKS_VERSION}_linux_arm64.tar.gz; \
    elif uname -m | grep armv6; then \
	curl -fsL -o /tmp/gitleaks.tar.gz \
	    $GITLEAKS_REPO/v$GITLEAKS_VERSION/gitleaks_${GITLEAKS_VERSION}_linux_armv6.tar.gz; \
    elif uname -m | grep armv7; then \
	curl -fsL -o /tmp/gitleaks.tar.gz \
	    $GITLEAKS_REPO/v$GITLEAKS_VERSION/gitleaks_${GITLEAKS_VERSION}_linux_armv7.tar.gz; \
    else \
	curl -fsL -o /tmp/gitleaks.tar.gz \
	    $GITLEAKS_REPO/v$GITLEAKS_VERSION/gitleaks_${GITLEAKS_VERSION}_linux_x64.tar.gz; \
    fi \
    && tar -C /usr/local/bin -xzf /tmp/gitleaks.tar.gz gitleaks \
    && chmod +x /usr/local/bin/gitleaks \
    && apt-get remove -y curl \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
